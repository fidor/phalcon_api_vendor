<?php

namespace Api\v1\Controllers;

use InnovationDotHome\Api\BaseApiController;

class ExampleApiController extends BaseApiController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function show($id)
    {

    }

    public function save()
    {

    }

    public function login()
    {

    }

    public function socialLogin()
    {

    }

    public function getEmail()
    {

    }

    public function register()
    {
        if (empty($this->requestData->email)) {
            return $this->setJsonResponse(400, 'EMAIL_EMPTY');
        }

        if (!filter_var($this->requestData->email, FILTER_VALIDATE_EMAIL)) {
            return $this->setJsonResponse(400, 'EMAIL_INVALID');
        }

        if (empty($this->requestData->password)) {
            return $this->setJsonResponse(400, 'PASSWORD_EMPTY');
        }

        if (strlen($this->requestData->password) < 6) {
            return $this->setJsonResponse(400, 'PASSWORD_LENGTH_INVALID');
        }

        if (empty($this->requestData->confirmPassword)) {
            return $this->setJsonResponse(400, 'CONFIRMATION_PASSWORD_EMPTY');
        }

        if ($this->requestData->password != $this->requestData->confirmPassword) {
            return $this->setJsonResponse(400, 'PASSWORDS_DONT_MATCH');
        }

        $user = Users::findFirstByEmail($this->requestData->email);

        if (!empty($user->id)) {
            return $this->setJsonResponse(400, 'USER_EXISTS');
        }

        $user = new Users();
        $user->token = uniqid();
        $user->email = $this->requestData->email;
        $user->password = md5(_COOKIE_KEY_ . $this->requestData->password);
        $user->dateCreated = date('Y-m-d H:i:s');
        $user->dateUpdated = date('Y-m-d H:i:s');

        if ($user->save() == true) {
            $data = [
                'id' => $user->id,
                'token' => $user->token
            ];

            return $this->setJsonResponse(200, 'REGISTERED_SUCCESSFULLY', $data);
        }

        return $this->setJsonResponse(500, 'SERVER_ERROR');
    }

    public function restore()
    {

    }

    public function updateAvatar($id)
    {

    }
}
