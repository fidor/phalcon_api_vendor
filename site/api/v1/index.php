<?php

define('LOCALE_ID_DEFAULT', 1);
define('SERVER_IP', $_SERVER['SERVER_ADDR']);
define('_COOKIE_KEY_', 'cfGrYEaCGQFDoisrXNO2kzQ3aRchJI6qVUbuWrBEtjf9TtFedYOvizQ3');
define('CORE_PATH', __DIR__ . '/../../app/' );

require '../../vendor/autoload.php';

use Phalcon\Config;
use Phalcon\Loader as Loader;
use Phalcon\Mvc\Micro as MvcMicro;
use Phalcon\DI\FactoryDefault as FactoryDefault;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

$loader = new Loader();
$loader->registerDirs([
    CORE_PATH . 'controllers/',
    CORE_PATH . 'Models/',
])->register();

$di = new FactoryDefault();

// Set config
$config = new Config(require CORE_PATH . 'config/config.php');

if (is_readable(CORE_PATH . 'config/config_dev.php')) {
    $override = new Config(require CORE_PATH . 'config/config_dev.php');
    $config->merge($override);
}

$di->set('config', $config);

$di['db'] = function() use ($di) {
    return new DbAdapter((array) $di->get('config')->get('database'));
};

$app = new MvcMicro($di);

$usersCollection = new MicroCollection();

$usersCollection->setHandler('UsersController', true);

$usersCollection->post('/users/login', 'loginAction');
$usersCollection->post('/users', 'registrationAction');
$usersCollection->get('/users/{id:[0-9]+}', 'showAction');
$usersCollection->patch('/users/{id:[0-9]+}', 'saveAction');
$usersCollection->post('/users/email', 'emailAction');
$usersCollection->post('/users/restore', 'restoreAction');
$usersCollection->post('/users/avatar/{id:[0-9]+}', 'avatarAction');

$app->mount($usersCollection);

$app->notFound(function () use ($app) {
    $response = new Phalcon\Http\Response();
    $response->setJsonContent(array('status' => 404, 'message' => 'Page was not found!'));
    $response->setStatusCode(404, 'Page was not found!');
    return $response;
});

$app->get('/', function () {
    $response = new Phalcon\Http\Response();
    $response->setContent(file_get_contents('index.html'));

    return $response;
});

$app->handle();

saveRequest($di);

function saveRequest($di)
{
    $request = $di->get('request');
    $data = $request->getJsonRawBody();

    $uri = $request->getURI();

    $content = date('Y-m-d H:i:s') . "\n";
    $content .= "URI: $uri \n";
    $file = sys_get_temp_dir() . "/api.log";

    if (isset($_SERVER['REMOTE_ADDR'])) {
        $content .= $_SERVER['REMOTE_ADDR'] . "\n";
    }
    $content .= "Requset:\n";

    if ($data && is_object($data)) {
        foreach ($data as $i => $a) {
            $content .= $i . ": " . (is_array($a) ? print_r($a, true) : $a) . "\n";
        }
    }
    
    $response = $di->get('response')->getContent();
    $content .= "Response:\n" . substr($response, 0, 1000) . "\n";
    
    $content .= "====================================\n";
    file_put_contents($file, $content, FILE_APPEND);
}