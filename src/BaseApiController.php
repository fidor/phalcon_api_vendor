<?php

namespace InnovationDotHome\Api;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Di;

abstract class BaseApiController extends Controller
{
    protected $response;
    protected $requestData;
    protected $token;

    protected function onConstruct()
    {
        $this->response = new Response();
        if ( $this->request->isGet() ) {
            $this->requestData = $this->request->getQuery();
        }
        if ( $this->request->isPost() || $this->request->isPut() ) {
            $this->requestData = $this->request->getJsonRawBody();
            if ( $this->request->hasFiles() ) {
                $this->token = $this->request->getPost( "token", "alphanum", null );
            } else {
                $this->token = ( !empty( $this->requestData->token ) ) ? $this->requestData->token : null;
            }
        }
    }

    public function setJsonResponse( $status = 200, $message = '', $data = [] )
    {
        $this->response->setStatusCode( $status, $message );
        $this->response->setContentType( 'application/json', 'UTF-8' );
        $this->response->setJsonContent( [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ] );

        return $this->response;
    }

    public static function checkVersion()
    {
        $di = new Di();
        return $di::getDefault()->getConfig()->api->appVersion->toArray();
    }

}
