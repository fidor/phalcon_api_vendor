<?php

namespace InnovationDotHome\Api;

use Supermodule\ControllerBase as SupermoduleBase;
use Phalcon\Mvc\Controller;

abstract class Api extends Controller
{
    protected $dataReturn = true;

    public function initialize()
    {
        if (!SupermoduleBase::checkAndConnectModule('api')) {
            $this->dataReturn = false;
        }
    }
}
